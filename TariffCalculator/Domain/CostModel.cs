﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TariffCalculator.Domain
{
    public class CostModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int Basis { get; set; }

        public int ProductId { get; set; }

        public int? Allowance { get; set; }

        public decimal KWHPrice { get; set; }
        public decimal Cost { get; set; }

        [ForeignKey(nameof(ProductId))]
        public Product Product { get; set; }

        public decimal CalculatePrice(int consumption)
        {
            var baseCost = Cost * Basis;

            if (consumption < 0) return baseCost;

            if (Allowance != null)
                consumption -= Allowance.Value;

            decimal tariff = baseCost + consumption * KWHPrice;

            if (Allowance != null && consumption > Allowance.Value)
                tariff += (consumption - Allowance.Value) * KWHPrice;

            return tariff;
        }
    }
}
