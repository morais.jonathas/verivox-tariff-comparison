﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TariffCalculator.Contracts.v1.Response;

namespace TariffCalculator.Services
{
    public interface IProductService
    {
        Task<List<ProductResponse>> CalculatePriceAsync(int consumption);
    }
}