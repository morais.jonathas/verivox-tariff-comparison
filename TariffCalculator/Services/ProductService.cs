﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TariffCalculator.Contracts.v1.Response;
using TariffCalculator.Data;

namespace TariffCalculator.Services
{
    public class ProductService : IProductService
    {
        private readonly DataContext _dataContext;

        public ProductService(DataContext dataContext) => _dataContext = dataContext;

        public async Task<List<ProductResponse>> CalculatePriceAsync(int consumption)
        {
            return await _dataContext.Products.Select(p => new ProductResponse
            {
                TariffName = p.Name,
                AnnualCosts = p.CostModel.CalculatePrice(consumption).ToString()
            }).ToListAsync();
        }
    }
}