﻿namespace TariffCalculator.Contracts.v1.Response
{
    public class ProductResponse
    {
        /// <summary>
        /// Tariff Name
        /// </summary>
        public string TariffName { get; set; }

        private string _annualCosts;

        /// <summary>
        /// Annual Costs
        /// </summary>
        public string AnnualCosts
        {
            get { return $"{_annualCosts} (€/year)"; }
            set { _annualCosts = value; }
        }
    }
}
