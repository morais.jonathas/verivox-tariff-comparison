﻿using Microsoft.AspNetCore.Mvc;

namespace TariffCalculator.Contracts.v1.Requests
{
    public class GetProductInput
    {
        [FromRoute(Name = "comsumption")]
        public int Consumption { get; set; }
    }
}
