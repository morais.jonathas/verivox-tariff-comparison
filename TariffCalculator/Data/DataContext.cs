﻿using Microsoft.EntityFrameworkCore;
using TariffCalculator.Domain;

namespace TariffCalculator.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<Product> Products { get; set; }

        public DbSet<CostModel> CostModels { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(DataContext).Assembly);
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    // local path for the Sqlite database
        //    optionsBuilder.UseSqlite("Filename=./products_comparison.db");
        //}
    }
}
