﻿using System.Linq;
using TariffCalculator.Domain;

namespace TariffCalculator.Data
{
    public class DBInitializer
    {
        private const string ProductA = "Basic electricity tariff";
        private const string ProductB = "Packaged tariff";

        public static void Initialize(DataContext context)
        {
            var initializer = new DBInitializer();
            initializer
                .SeedProducts(context)
                .SeedCostModels(context);
                
        }

        public DBInitializer SeedProducts(DataContext context)
        {
            if (!context.Products.Any())
            {

                var products = new[] {
                    new Product {
                        Id = 1,
                        Name = ProductA
                    },
                    new Product {
                        Id = 2,
                        Name = ProductB
                    }
                };
                context.Products.AddRange(products);

                context.SaveChanges();
            }

            return this;
        }

        public void SeedCostModels(DataContext context)
        {
            if (!context.CostModels.Any())
            {
                var costModels = new[] {
                    new CostModel {
                        ProductId = context.Products.Where(x => x.Name == ProductA).Select(p => p.Id).FirstOrDefault(),
                        Cost = 5m,
                        Basis = 12,
                        KWHPrice = 0.22m
                    },
                    new CostModel {
                        ProductId = context.Products.Where(x => x.Name == ProductB).Select(p => p.Id).FirstOrDefault(),
                        Cost = 800m / 12,
                        Basis = 12,
                        Allowance = 4000,
                        KWHPrice = 0.30m
                    }
                };
                context.CostModels.AddRange(costModels);

                context.SaveChanges();
            }
        }
    }
}
