﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TariffCalculator.Data;
using TariffCalculator.Services;

namespace TariffCalculator.Installers
{
    public class DbInstaller : IInstaller
    {
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<DataContext>(options =>
            {
                var defaultConnection = configuration.GetSection("ConnectionStrings");
                options.UseSqlite(defaultConnection["DefaultConnection"]);

                //options.UseInMemoryDatabase(connectionString);
            });

            services.AddScoped<IProductService, ProductService>();
        }
    }
}
