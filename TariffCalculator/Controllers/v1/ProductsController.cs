﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TariffCalculator.Contracts.v1.Requests;
using TariffCalculator.Contracts.v1.Response;
using TariffCalculator.Services;
using static TariffCalculator.Contracts.v1.ApiRoutes;

namespace TariffCalculator.Controllers.v1
{
    public class ProductsController : Controller
    {
        private readonly IProductService _productService;

        public ProductsController(IProductService postService)
            => _productService = postService;

        [HttpGet(Products.GetProduct)]
        [ProducesResponseType(200, Type = typeof(ProductResponse))]
        public async Task<IActionResult> GetProduct(int consumption)
            => Ok(await _productService.CalculatePriceAsync(consumption));
    }
}